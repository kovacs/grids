from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()
from grids import views

urlpatterns = patterns('',
    url(r'^grids/$', 'grids.views.index'),
    url(r'^grids/create/$', 'grids.views.create'),
    url(r'^grids/detail/(?P<grid_id>\d+)/$', 'grids.views.detail'),

		url(r'^admin/', include(admin.site.urls)),
)
