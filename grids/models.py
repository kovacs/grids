from django.db import models
from django.core.validators import MinValueValidator
from django.core.validators import MaxValueValidator

class Grid (models.Model):
	offset = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(100)])
