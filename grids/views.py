from django.shortcuts import render
from django.http import HttpResponse
from grids.models import Grid
from django.template import Context, loader
from django.template import RequestContext
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from grids.utils import render_grid
from django.core.exceptions import ValidationError

# Create your views here.
def index(request):
		grid_list = Grid.objects.order_by('-pk')[:5]
		return render_to_response("grids/index.html", {'grid_list': grid_list},  context_instance=RequestContext(request))

def create(request):
		offset = request.POST['offset']
		g = Grid(offset=offset)
		try:
			g.full_clean()
			g.save()
			return redirect('/grids/detail/' + str(g.id))
		except ValidationError:
			return redirect("/grids")

def detail(request, grid_id):
		g = get_object_or_404(Grid, pk=grid_id)
		return render_to_response('grids/detail.html', {'values': render_grid(g)}, RequestContext(request))