from grids.models import Grid

def render_grid(g):
	values = []
	row = []
	for i in range(1, 100):
		if (i - 1) % 10 == 0:
			values.append(row)
			row = []
		if i % int(g.offset) == 0:
			row.append('hit') 
		else:
			row.append('nohit')
	return values
